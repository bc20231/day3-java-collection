package com.thoughtworks.collection;

import java.util.List;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream()
                .reduce(0, (number1, number2) -> number2 % 2 != 0 ? number2 : number1);
    }

    public String getLongest(List<String> words) {
        return words.stream()
                .reduce("", (word1, word2) -> word2.length() > word1.length() ? word2 : word1);
    }

    public int getTotalLength(List<String> words) {
        return words.stream()
                .reduce(0, (totalLength, word) -> totalLength + word.length(), Integer::sum);
    }
}
